//
//  WeatherData.swift
//  Weather_W_S
//
//  Created by TSTuser on 5/5/19.
//  Copyright © 2019 Oles Bychevsky. All rights reserved.
//

import UIKit

class WeatherData {
    
    //Model variables
    
    var temperature : Int = 0
    var condition : Int = 0
    var city : String = ""
    var weatherIconName : String = ""
    //To match condition code to the name of image
    
        func updateWeatherIcon(condition: Int) -> String {
    
        switch (condition) {
    
            case 0...300 :
                return "storm1"
    
            case 301...500 :
                return "light_rain"
    
            case 501...600 :
                return "shower1"
    
            case 601...700 :
                return "snow1"
    
            case 701...771 :
                return "fog"
    
            case 772...799 :
                return "storm2"
    
            case 800 :
                return "sunny"
    
            case 801...804 :
                return "cloudy1"
    
            case 900...903, 905...1000  :
                return "storm2"
    
            case 903 :
                return "snow2"
    
            case 904 :
                return "sunny"
    
            default :
                return "dunno"
            }
    
        }
    
}
