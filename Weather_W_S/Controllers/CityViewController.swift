//
//  CityViewController.swift
//  Weather_W_S
//
//  Created by TSTuser on 5/5/19.
//  Copyright © 2019 Oles Bychevsky. All rights reserved.
//

import UIKit

protocol ChangeCityDelegate {
    func userEnteredANewCityName (city : String)
}

class CityViewController: UIViewController {
    
    var delegate : ChangeCityDelegate?
    
    @IBOutlet weak var cityTextField: UITextField!
    
    @IBAction func getWeather(_ sender: Any) {
        
        let cityName = cityTextField.text! //to get a city name entered in the text field
        delegate?.userEnteredANewCityName(city: cityName) //call the method userEnteredANewCityName
        self.dismiss(animated: true, completion: nil) //go back to WeatherViewController
    }
    
    @IBAction func backButtonPressed(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
